//
//  ListViewController.swift
//  OnboardingExample
//
//  Created by Константин Грушин on 16.01.2020.
//  Copyright © 2020 Anitaa. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var model: ModelController!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var celsiusLabel: UIButton!
    @IBOutlet weak var farenheitLabel: UIButton!
    
    var tableHeight: CGFloat = 0
    var isCelsius: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        tableHeight = 0
        celsiusLabel.setTitleColor(.red , for: .normal)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        let offset: CGFloat = 30 // странно, высота ячейки 44.0 все равно нужен offset
        
        cell?.textLabel?.text = model.slides[indexPath.row].city
        cell?.detailTextLabel?.text = self.isCelsius ?
            "\(model.slides[indexPath.row].tempC)˚" :
            "\(model.slides[indexPath.row].tempF)˚"
        
        tableHeight += cell!.frame.height
        tableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: tableHeight + offset)

        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.slides.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(model.slides.count)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            model.slides.remove(at: indexPath.row)
        }
        tableView.deleteRows(at: [indexPath], with: .fade)
        reloadTable()
    }
    
    @IBAction func setCelsius(_ sender: UIButton) {
        self.isCelsius = true
        if isCelsius {
            celsiusLabel.setTitleColor(.red , for: .normal)
            farenheitLabel.setTitleColor(.gray , for: .normal)
        }
        reloadTable()
    }
    
    @IBAction func setFarenheit(_ sender: UIButton) {
        self.isCelsius = false
        if !isCelsius {
            farenheitLabel.setTitleColor(.red , for: .normal)
            celsiusLabel.setTitleColor(.gray , for: .normal)
        }
        reloadTable()
    }
    
    func reloadTable() {
        tableHeight = 0
        tableView.reloadData()
    }
    
    // segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let searchCityWeatherController = segue.destination as? SearchCityWeatherController {
            searchCityWeatherController.model = model
        }
    }
    
    @IBAction func goToSearchCItyWeatherPage(_ sender: UIButton) {
        performSegue(withIdentifier: "searchCityWeather", sender: self)
    }
}

