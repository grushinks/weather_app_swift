//
//  SearchCityWeatherController.swift
//  OnboardingExample
//
//  Created by Константин Грушин on 21.01.2020.
//  Copyright © 2020 Anitaa. All rights reserved.
//

import UIKit

enum ConvertTemperature {
    case Сelsius(temp: String)
    case Farenheit(temp: String)
    
    var convert: Int {
        switch self {
        case .Сelsius(let temp):
            return Int(Double(temp)! - 273.15)
        case .Farenheit(let temp):
            return Int((Double(temp)! - 273.15) * 9/5 + 32)
        }
    }
}



class SearchCityWeatherController: UIViewController {

    var model: ModelController!
    
    @IBOutlet weak var searchCity: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let listViewController = segue.destination as? ListViewController {
            listViewController.model = model
        }
        
        if let mainPageViewController = segue.destination as? MainPage {
            mainPageViewController.model = model
        }
    }
    

    @IBAction func searchCityWeather(_ sender: UIButton) {
        if (searchCity.text != "") {
            
            let weatherManager = ApiWeatherManager()
            weatherManager.getCityWeather(cityName: searchCity.text!,
                               onSuccsess: { (json, response) in
                                DispatchQueue.main.async {
                                switch response.statusCode {
                                case(200...399):
                                    self.addDataToTable(json: json)
                                case(400...526):
                                    self.alertError(error: json)
                                default: break
                                }
                            }
            },
                onFalure: { error in
                    self.alertError(error: error as! Dictionary<String, AnyObject>)
            })
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        performSegue(withIdentifier: "mainPage", sender: self)
    }
    
    func addDataToTable(json: [String: AnyObject]){
        var similar: Bool = false
        model.slides.forEach{ element in
            if(element.city == searchCity.text!) {
                let error = ["message": "такой город уже есть в вашем списке"]
                self.alertError(error: error)
                similar = true
            }
        }

        if !similar {
            let temp = String(format: "%@", json["main"]!["temp"]!! as! CVarArg)
            
            model.slides.append(
                Weather(
                    tempF: ConvertTemperature.Farenheit(temp: temp).convert,
                    tempC: ConvertTemperature.Сelsius(temp: temp).convert,
                    city: searchCity.text!
                )
            )
            performSegue(withIdentifier: "listCityWeather", sender: self)
            
        }
    }
    
    func alertError(error: Dictionary<String, Any>) {
        let alertController = UIAlertController(title: "Alert", message: error["message"]! as? String, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { _ in self.someHandler() } )
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    // хендлер на кнопку "ok"
    func someHandler() {
        print("someHandler")
    }
}
