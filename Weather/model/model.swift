//
//  model.swift
//  OnboardingExample
//
//  Created by Константин Грушин on 18.01.2020.
//  Copyright © 2020 Anitaa. All rights reserved.
//

import Foundation

class ModelController {
    var slides: [Weather] = [
        Weather(tempF: 70, tempC: 21, city: "Amsterdam"),
        Weather(tempF: 65 , tempC: 18, city: "London"),
        Weather(tempF: 77, tempC: 25, city: "Paris"),
    ]
}

struct Weather {
    var tempF: Int
    var tempC: Int
    var city: String
    
    init(tempF:Int, tempC: Int, city: String) {
        self.tempF = tempF
        self.tempC = tempC
        self.city = city
    }
}
