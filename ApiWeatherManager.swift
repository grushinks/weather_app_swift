//
//  File.swift
//  Weather
//
//  Created by Константин Грушин on 28.01.2020.
//  Copyright © 2020 Anitaa. All rights reserved.
//

import Foundation


protocol FinalURLPoint {
    var baseURL: URL { get }
    var path: String { get }
    var request: URLRequest { get }
}

let apiKey: String = "559cb2e553d59c2ab164ec6bb2c7c8a6"

enum ForecastType: FinalURLPoint {
    
    case Current(cityName: String)
    
    var baseURL: URL {
        return URL(string: "https://api.openweathermap.org")!
    }
    
    var path: String {
        switch self {
        case .Current(let cityName):
            return "/data/2.5/weather?q=\(cityName)&appid=\(apiKey)"
        }
    }
    
    var request: URLRequest {
        let url = URL(string: path, relativeTo: baseURL)
        return URLRequest(url: url!)
    }
}

class ApiWeatherManager {
    
    func getCityWeather(cityName: String, onSuccsess: @escaping([String: AnyObject], HTTPURLResponse) -> Void, onFalure: @escaping(Error) -> Void) {
        let request = ForecastType.Current(cityName: cityName).request
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let HTTPResponse = response as? HTTPURLResponse else {
                onFalure(error!)
                return
            }
            
            if data == nil {
                if let error = error {
                    onFalure(error)
                }
            } else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject]
                    onSuccsess(json!, HTTPResponse)
                } catch let error as NSError {
                    onFalure(error)
                }
            }
        }
        task.resume()
    }
    
}
